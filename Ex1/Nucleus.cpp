#include "Nucleus.h"
using namespace std;
#include <iostream>
#include <string>


void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}


int Gene::get_start()
{
	return this->_start;
}


int Gene::get_end()
{
	return this->_end;
}


bool Gene::is_on_complementary_dna_strand()
{
	return this->_on_complementary_dna_strand;
}


void Nucleus::init(const std::string dna_sequence)
{
	this->_DNA_strand = dna_sequence;
	this->_complementary_DNA_strand = "";

	for (int i = 0; i < dna_sequence.length(); i++)
	{
		switch (dna_sequence[i])
		{
		case 'G':
			this->_complementary_DNA_strand += "C";
			break;
		case 'C':
			this->_complementary_DNA_strand += "G";
			break;
		case 'T':
			this->_complementary_DNA_strand += "A";
			break;
		case 'A':
			this->_complementary_DNA_strand += "T";
			break;
		default:
			cerr << "Nucleotoide doesnt exist" << endl;
			_exit(1);
			break;
		}
		
	}
}

string Nucleus::get_RNA_transcript(Gene& gene) const
{
	string takeFrom = "", res = "";
	
	if (gene.is_on_complementary_dna_strand())
	{
		takeFrom = this->_complementary_DNA_strand;
	}
	else
	{
		takeFrom = this->_DNA_strand;
	}

	for (int i = gene.get_start(); i <= gene.get_end(); i++)
	{
		if (takeFrom[i] == 'T')
		{
			res += 'U';
		}
		else
		{
			res += takeFrom[i];
		}
	}

	return res;
}

string Nucleus::get_reversed_DNA_strand() const
{
	string res = "";

	for (int i = this->_DNA_strand.length() - 1; i >= 0 ; i--)
	{
		res += this->_DNA_strand[i];
	}

	return res;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	return this->_DNA_strand.find(codon);
}