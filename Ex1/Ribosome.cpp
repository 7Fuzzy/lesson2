#include <string>
#include <string>
#include <iostream>
#include "Ribosome.h"
#include "Protein.h"
using namespace std;

#define NUC_NUM 3

Protein* Ribosome::create_protein(std::string &RNA_transcript) const
{
	Protein* res = new Protein;
	res->init();
	while (RNA_transcript.length() >= NUC_NUM)
	{
		string findAmino = RNA_transcript.substr(0, NUC_NUM);
		RNA_transcript.erase(0, NUC_NUM);
		AminoAcid toAdd = get_amino_acid(findAmino);
		if (toAdd == UNKNOWN)
		{
			res->clear();
			return nullptr;
		}
		res->add(toAdd);
	}

	return res;
}