#include <string>
#include <iostream>
#include "Protein.h"
#include "Mitochondrion.h"
using namespace std;

#define GLOCUSE_LEN 7
#define MIN_GLUC_LEVEL 50

void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	AminoAcid glocuse[] = { ALANINE, LEUCINE, GLYCINE, HISTIDINE, LEUCINE, PHENYLALANINE, AMINO_CHAIN_END };
	int curr = 0;
	bool isValid = true;
	Protein prot = protein;

	for (int i = 0; i < GLOCUSE_LEN && isValid && prot.get_first(); i++)
	{
		AminoAcid currAmino = prot.get_first()->get_data();
		if (currAmino != glocuse[i])
		{
			isValid = false;
		}
		prot.set_first(prot.get_first()->get_next());
	}

	this->_has_glocuse_receptor = isValid;
}

bool Mitochondrion::produceATP(const int glocuse_unit) const
{
	return this->_has_glocuse_receptor && this->_glocuse_level >= MIN_GLUC_LEVEL;
}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}