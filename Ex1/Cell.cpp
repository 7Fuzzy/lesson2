#include <string>
#include <iostream>
#include "Protein.h"
#include "Mitochondrion.h"
#include "Nucleus.h"
#include "Ribosome.h"
#include "Cell.h"
using namespace std;

#define GLOC_LVL 50

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_mitochondrion.init();
	this->_nucleus.init(dna_sequence);
	this->_glocus_receptor_gene = glucose_receptor_gene;
	this->_atp_units = 0;
}

bool Cell::get_ATP()
{
	string transcript = this->_nucleus.get_RNA_transcript(this->_glocus_receptor_gene);
	Protein* prot = this->_ribosome.create_protein(transcript);
	if (prot == nullptr)
	{
		cerr << "Cant Create Protein" << endl;
		_exit(1);
	}
	this->_mitochondrion.insert_glucose_receptor(*prot);
	this->_mitochondrion.set_glucose(GLOC_LVL);
	if (this->_mitochondrion.produceATP(GLOC_LVL))
	{
		this->_atp_units = 100;
		return true;
	}
	return false;
}