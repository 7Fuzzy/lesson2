#pragma once
using namespace std;
#include <string>

class Gene
{
private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;
public:
	void init(const unsigned int start, const unsigned int end, const bool _on_complementary_dna_strand);
	int get_start();
	int get_end();
	bool is_on_complementary_dna_strand();
};

class Nucleus
{
private:
	string _DNA_strand;
	string _complementary_DNA_strand;

public:
	void init(const std::string dna_sequence);
	string get_RNA_transcript(Gene& gene) const;
	string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
};