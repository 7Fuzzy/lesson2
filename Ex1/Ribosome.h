#pragma once
#include <string>
#include <iostream>
#include "Protein.h"
using namespace std;

class Ribosome
{
public:
	Protein* create_protein(std::string &RNA_transcript) const;
};