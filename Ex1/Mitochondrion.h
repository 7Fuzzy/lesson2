#pragma once
#include <string>
#include <iostream>
#include "Protein.h"
using namespace std;

class Mitochondrion
{
private:
	int _glocuse_level;
	bool _has_glocuse_receptor;

public:
	void init();
	void insert_glucose_receptor(const Protein & protein);
	bool produceATP(const int glocuse_unit) const;
	void set_glucose(const unsigned int glocuse_units);
};